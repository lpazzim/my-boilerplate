// Button.test.js
import React from 'react';
import Button from '../Button';
import renderer from 'react-test-renderer';

test('Teste Button', () => {
  const component = renderer.create(
    <Button> Facebook</Button>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});