import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import PageOne from './containers/PageOne/PageOne';
import PageTwo from './containers/PageTwo/PageTwo.jsx';
import './App.scss';

const App = ({ user }) => (
  <HashRouter basename={process.env.PUBLIC_URL}>
      <div className="App">
        <Route user={user} path="/" exact component={PageOne} />
        <Route user={user} path="/two" exact component={PageTwo} />
      </div>
  </HashRouter>
);

export default App;
